
let body = document.getElementById('body');
let counter = 0;
document.addEventListener('click', function box(event){
    counter ++;
    let div = document.createElement('div');
    div.style.position='absolute';
    div.style.left=event.clientX+'px';
    div.style.top=event.clientY+'px';
    div.innerHTML=counter;
    let random = Math.random()*120
    let randomsize = Math.floor(Math.random() * (8 - 1 +1)+1)
    div.style.width= 50*randomsize+'px';
    div.style.height= 50*randomsize +'px';
    
    if (random < 20){
        div.style.backgroundColor='red';
    } else if(20 < random && random<40){
        div.style.backgroundColor='yellow';
    } else if(40 < random && random<60){
        div.style.backgroundColor='green';
    } else if(60 < random && random<80){
        div.style.backgroundColor='purple';
    } else if(80 < random && random<100){
        div.style.backgroundColor='brown';
    } else if(100 < random && random<120){
        div.style.backgroundColor='black';
    }
    body.appendChild(div);
})

let reset = document.getElementById('reset')
reset.addEventListener('click', function resetall(){
    body.innerHTML='';
})